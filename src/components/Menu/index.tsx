import React from "react";
import { Link } from "react-router-dom";

import menu from "../../lib/constants/menu";

function Menu() {
  return (
    <div>
      {menu.map((item) => (
        <Link to={item.link} key={item.name}>{item.name}</Link>
      ))}
    </div>
  );
}

export default Menu;
