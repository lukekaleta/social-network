import React from "react";

import Routing from "../routes";
import Menu from "./Menu";

function Root() {
  return (
    <div>
      <div className="header">
        <Menu />
      </div>
      <div className="content">
        <Routing />
      </div>
      <div className="footer" />
    </div>
  );
}

export default Root;
