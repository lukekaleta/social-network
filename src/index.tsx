import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";

import Root from "./components";

const Document = () => (
  <Router>
    <Root />
  </Router>
);

ReactDOM.render(
  <React.StrictMode>
    <Document />
  </React.StrictMode>,
  document.getElementById("root")
);
