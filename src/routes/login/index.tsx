import React, { useState } from "react";

import firebase from "../../lib/config/firebase.config";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);

  async function login() {
    try {
      await setLoading(true);
      await firebase.auth().signInWithEmailAndPassword(email, password);
      await firebase.auth().onAuthStateChanged((data) => console.log(data));
    } catch(err) {
      console.error(err.message);
    } finally {
      setLoading(false);
    }
  }

  return (
    <div>
      <div>
        <label>
          <span>E-mail</span>
          <input type="text" onChange={(e) => setEmail(e.target.value)} />
        </label>
      </div>
      <div>
        <span>Password</span>
        <label>
          <input
            type="password"
            onChange={(e) => setPassword(e.target.value)}
          />
        </label>
      </div>
      <div>
        <button onClick={login}>Login</button>
      </div>
      <div>{loading ? "Loading" : ""}</div>
    </div>
  );
}

export default Login;
