import React from "react";
import { Switch, Route } from "react-router-dom";

import router from "../lib/constants/router";

import Home from "./home";
import Login from "./login";

function Routing() {
  return (
    <Switch>
      <Route path={router.HOME} exact component={() => <Home />} />
      <Route path={router.LOGIN} exact component={() => <Login />} />
      <Route component={() => <h1>Page not found!</h1>} />
    </Switch>
  )
};

export default Routing;