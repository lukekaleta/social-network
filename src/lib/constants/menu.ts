import router from "./router";

export default [
  {
    name: "Home",
    link: router.HOME
  },  
  {
    name: "Login",
    link: router.LOGIN
  },
]