export default {
  HOME: "/",
  LOGIN: "/login",
  ABOUT: "/about",
  CONTACT: "/contact"
};