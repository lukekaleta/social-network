import firebase from "firebase";
import "firebase/auth";
import "firebase/database";

const config = {
  apiKey: "AIzaSyCsFN2zA0u6d8SKXQOxMZHok0OFfVT-XaI",
  authDomain: "social-network-af5ba.firebaseapp.com",
  databaseURL: "https://social-network-af5ba.firebaseio.com",
  projectId: "social-network-af5ba",
  storageBucket: "social-network-af5ba.appspot.com",
  messagingSenderId: "618576932703",
  appId: "1:618576932703:web:27917395d90234c78190ea",
};

firebase.initializeApp(config);
export default firebase;

export const database = firebase.database();
export const auth = firebase.auth();